<h1>Municipio de Quito - Análisis de sentimientos</h1>
Este proyecto abarca dos temas principales: análisis de sentimientos y extracción de tópicos, estos dos puntos se trabajan sobre tweets recolectados y relacionados al Municipio de Quito en Ecuador y sus instituciones. El análisis de sentimientos clasifica el tweet en uno de tres sentimientos (positivo, negativo y neutral). En cambio, la extracción de tópicos agrupa a todos los tweets y obtiene 'clusters' con las palabras más significativas que se relacionan entre ellas por similitud, con ello se deduce el tópico que define a la mayoría de las palabras. Finalmente, se creó un dashboard interactivo en el cual se puede filtrar los tweets por un rango de fecha. Además, se categorizó por sectores de la ciudad que fueron más mencionados en los tweets.


Este dashboard se puede visualizar en el siguiente enlace de Tableau: https://public.tableau.com/app/profile/eio5858/viz/SubtopicsLocations/Dashboard 

<img src="Dashboard.PNG" alt="Dashboard" style="height: 100px; width:100px;"/>
